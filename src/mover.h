//
// Created by blr on 19.07.16.
//

#ifndef CONTROLLER_MOVER_H
#define CONTROLLER_MOVER_H


#include <ros/ros.h>
//#include <tf2_ros/transform_listener.h>
//#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/OverrideRCIn.h>


class Mover {
public:
    Mover(ros::NodeHandle nh);
    ~Mover();
bool arm(bool value);
private:
    ros::Publisher _override;
    ros::Subscriber _pose_sub;
    ros::ServiceClient _arm_client;
    ros::Subscriber _state_sub;


    void setSafeArea(float x,float y,float z);
    void callBackPose(geometry_msgs::PoseStamped subscriber_msg);
    void callBackState(mavros_msgs::State state_msg);
    bool isWithinSafe(geometry_msgs::PoseStamped pose);
    void land();
    bool channelsAreValid(mavros_msgs::OverrideRCIn rc);



    int channels[8];
    float safe_pos_x;
    float safe_pos_y;
    float safe_pos_z;
    float safe_neg_x;
    float safe_neg_y;
    float safe_neg_z;
    float timeout;


    ros::Time last_request;
    geometry_msgs::PoseStamped _pose;
    mavros_msgs::State _current_state;
    mavros_msgs::CommandBool arm_cmd;
    mavros_msgs::OverrideRCIn rc_override;
};


#endif //CONTROLLER_MOVER_H
