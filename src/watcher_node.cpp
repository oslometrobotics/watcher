#include <ros/ros.h>


#include "watcher.h"


int main(int argc, char *argv[]) {
    //initilize node
    ros::init(argc, argv, "watcher");

    ros::NodeHandle nh("~");
    ros::Duration(5.0).sleep();
    //create wathcer object
    Watcher watcher(nh);
    ROS_INFO_STREAM("made watcher");
    // wait for 9 seconds
    ros::Duration(9.0).sleep();
    while (nh.ok()) {
        watcher.watch(nh);
    }
    return 0;
}



