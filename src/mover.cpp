//
// Created by blr on 19.07.16.
//
#include <mavros/utils.h>
#include <mavros/mavlink_diag.h>
#include <mavconn/interface.h>
#include "mover.h"

//Constructor
Mover::Mover(ros::NodeHandle nh) {

    _state_sub = nh.subscribe<mavros_msgs::State>("/mavros_red/state", 1, &Mover::callBackState, this);
    _pose_sub = nh.subscribe<geometry_msgs::PoseStamped>("/qualisys/papp/pose", 1, &Mover::callBackPose, this);
    _arm_client = nh.serviceClient<mavros_msgs::CommandBool>("/mavros_red/cmd/arming");
    _override = nh.advertise<mavros_msgs::OverrideRCIn>("/mavros_red/override", 10);
    last_request = ros::Time::now();

    nh.param("x", safe_pos_x, 1.0f);
    nh.param("neg_x", (safe_neg_x), -1.0f);
    nh.param("y", safe_pos_y, 1.0f);
    nh.param("neg_y", (safe_neg_y), -1.0f);
    nh.param("y", safe_pos_z, 1.0f);
    nh.param("neg_y", (safe_neg_z), -1.0f);
    nh.param("timeout", (timeout), 0.5f);

    nh.param("roll", channels[0], 1500);
    nh.param("pitch", channels[1], 1500);
    nh.param("thrust", channels[2], 1000);
    nh.param("yaw", channels[3], 1500);
    nh.param("ch5", channels[4], 1500);
    nh.param("ch6", channels[5], 1500);
    nh.param("ch7", channels[6], 1500);
    nh.param("ch8", channels[7], 1500);


    ROS_INFO_STREAM(std::endl
                            << "[CONTROLLER PARAMETERS]" << std::endl
                            << "x			: " << safe_pos_x << std::endl
                            << "-x		        :" << safe_neg_x << std::endl
                            << "y			: " << safe_pos_y << std::endl
                            << "-y		        :" << safe_neg_y << std::endl
                            << "z			: " << safe_pos_z << std::endl
                            << "-z		        :" << safe_neg_z << std::endl
                            << "timeout                 :" << timeout << std::endl
    );
    ROS_INFO_STREAM("intizialised mover");
}

Mover::~Mover() {}

//Callback function that gets executed every time it gets a new state message
void Mover::callBackState(mavros_msgs::State state_msg) {
    _current_state = state_msg;
    if (ros::Time::now() - last_request > ros::Duration(timeout)) {
        ROS_INFO_STREAM("No data ; Disarming");
        arm(false);
    }


}

//Callback function that gets executed every time it gets a new pose message
void Mover::callBackPose(geometry_msgs::PoseStamped subscriber_msg) {
    last_request = ros::Time::now();
    _pose = subscriber_msg;
    if (!isWithinSafe(_pose)) {
        if (arm(false)) { ROS_INFO_STREAM_THROTTLE(0.3, "Out of bounds. Panic. Land?"); }
    }

    if (channelsAreValid(rc_override)) {
        for (int i = 0; i < 8; ++i) {
            rc_override.channels[i] = channels[i];
        }
        _override.publish(rc_override);

        //ROS_INFO_STREAM("");

        mavlink::
    }

}

bool Mover::isWithinSafe(geometry_msgs::PoseStamped pose) {
    return (pose.pose.position.x <= safe_pos_x) && (pose.pose.position.x >= safe_neg_x) &&
           (pose.pose.position.y <= safe_pos_y) && (pose.pose.position.y >= safe_neg_y) &&
           (pose.pose.position.z <= safe_pos_z) && (pose.pose.position.z >= safe_neg_z);
}


bool Mover::arm(bool value) {
    //Create a message for arming/disarming
    arm_cmd.request.value = value;
    //Call the service
    _arm_client.call(arm_cmd);
    return arm_cmd.response.success;
}

bool Mover::channelsAreValid(mavros_msgs::OverrideRCIn rc) {
    for (int i = 0; i < 8; ++i) {
        rc.channels[i] = channels[i];
    }
    for (int i = 0; i < 8; ++i) {
      //  ROS_INFO_STREAM("channel ch" << i << " value: " << rc.channels[i]);

        if ((rc.channels[i] == 65535) || ((rc.channels[i] >= 998) && (rc.channels[i] <= 1990))) {
            // if (!((rc.channels[i] >= 998) && (rc.channels[i] <= 1990)||(rc.channels[i] == 65535))) {


        }
        else{
            ROS_INFO_STREAM("channel ch" << i << " is invalid: " << rc.channels[i]);
            return false ;
        }

    }

    return true;
}



//TODO
void Mover::land() {


}
