//
// Created by blr on 26.07.16.
//

#include "watcher.h"


Watcher::Watcher(ros::NodeHandle nh) : tfListener(tfBuffer) {
    nh_ = nh;
    nh.getParam("drone_name", drone_name);
    if (nh.hasParam("drone_name")) {
        ros::service::waitForService("/mavros_" + drone_name + "/cmd/arming");
        _arm_client = nh.serviceClient<mavros_msgs::CommandBool>("/mavros_" + drone_name + "/cmd/arming", true);
        _mode_client = nh.serviceClient<mavros_msgs::SetMode>("/mavros_" + drone_name + "/set_mode");
    }
    nh.param("x", safe_zone.pos_x, 0.5f);
    nh.param("neg_x", (safe_zone.neg_x), -0.5f);
    nh.param("y", safe_zone.pos_y, 0.5f);
    nh.param("neg_y", (safe_zone.neg_y), 0.5f);
    nh.param("z", safe_zone.pos_z, 1.0f);
    nh.param("neg_z", (safe_zone.neg_z), -0.5f);
    nh.param("timeout", (timeout), 1.0f);
    last_request = ros::Time::now();
    last_arm = ros::Time::now();
    last_shutdown = ros::Time::now();

}

Watcher::~Watcher() {
}

/* Main function to check and react if the drone is outside the defined area
 * and is getting new transforms */
void Watcher::watch(ros::NodeHandle nh) {
//    mode.request.custom_mode = "STABILIZE";
//    ros::Duration dur = ros::Duration(10.0);
//    _mode_client.waitForExistence(dur);
//    _mode_client.call(mode);
    geometry_msgs::TransformStamped transform_stamped;

    try {
        transform_stamped = tfBuffer.lookupTransform(drone_name, "origo",
                                                     ros::Time(0));

    }
    catch (tf2::TransformException &ex) {
        ROS_WARN("%s", ex.what());
        ros::Duration(1.0).sleep();
    }

    if (std::isnan(transform_stamped.transform.translation.x)) ROS_WARN("x is NaN");

    if (!isWithinSafe(transform_stamped)) {
        //ROS_WARN_THROTTLE(0.3, "%s out of bounds. Panic.", drone_name.c_str());
        ROS_WARN_THROTTLE(0.1, "%s out of bounds. Panic.", drone_name.c_str());
        if (arm(false, nh) == true) {
            ROS_WARN("%s disarmed.", drone_name.c_str());
        }
        else
            ROS_WARN("OOB; failed to  disarmed %s .", drone_name.c_str());
    }
    if (transformUnchanged) {
        //check timer;
        if (ros::Time::now() - last_request > ros::Duration(timeout)) {
            ROS_WARN_THROTTLE(0.1, "%s timed out. Disarm and panic.", drone_name.c_str());
            //last_request = ros::Time::now();
            if (arm(false, nh) == true) {
                ROS_WARN("%s disarmed.", drone_name.c_str());
            }
            else
                ROS_WARN("TO; failed to  disarmed %s.", drone_name.c_str());
        }
    }

    if ((transform_stamped.transform.translation.x == last_transform_stamped.transform.translation.x)) {
        if (transformUnchanged == false) last_request = ros::Time::now();
        transformUnchanged = true;
    }
    else {
        transformUnchanged = false;
    }
    last_transform_stamped = transform_stamped;


}

/* Help function to check if a transform is within
 * the defined safe area struct    */
bool Watcher::isWithinSafe(geometry_msgs::TransformStamped transformStamped) {
    return (transformStamped.transform.translation.x <= safe_zone.pos_x) &&
           (transformStamped.transform.translation.x >= safe_zone.neg_x) &&
           (transformStamped.transform.translation.y <= safe_zone.pos_y) &&
           (transformStamped.transform.translation.y >= safe_zone.neg_y) &&
           (transformStamped.transform.translation.z <= safe_zone.pos_z) &&
           (transformStamped.transform.translation.z >= safe_zone.neg_z);
}


/* Method for triggering arming and disarming service in MAVROS
 *
 * Takes a boolean value which arms the service
 * for TRUE and disarms for FALSE */
bool Watcher::arm(bool value, ros::NodeHandle nh) {
    //Create a message for arming/disarming
    arm_cmd.request.value = value;
    //Call the service
    ROS_INFO_STREAM("Calling disarm");
    _arm_client.call(arm_cmd);
    if (arm_cmd.response.success) ROS_INFO_STREAM("success");
    else {
        ROS_INFO_STREAM("failed, setting LAND mode");
        mode.request.custom_mode = "LAND";
        _mode_client.call(mode);
    }
    return arm_cmd.response.success;
}

