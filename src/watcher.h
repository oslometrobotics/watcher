//
// Created by blr on 26.07.16.
//

#ifndef CONTROLLER_WATCHER_H
#define CONTROLLER_WATCHER_H



#include <ros/ros.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/OverrideRCIn.h>
#include <mavros_msgs/SetMode.h>
#include <tf2_ros/transform_listener.h>
#include <string>

//Safezone box variables in meters
struct SafeZone{
    float pos_x;
    float neg_x;
    float pos_y;
    float neg_y;
    float pos_z;
    float neg_z;

};


class Watcher {
public:
    Watcher(ros::NodeHandle nh);
    ~Watcher();
    void watch(ros::NodeHandle nh);

private:

    bool arm(bool value,ros::NodeHandle nh);
    bool isWithinSafe(geometry_msgs::TransformStamped transformStamped);
    bool transformUnchanged;
    float timeout;

    int counter;
    ros::NodeHandle nh_;
    SafeZone safe_zone;
    ros::Time last_shutdown;
    ros::Time last_request;
    ros::Time last_arm;
    geometry_msgs::TransformStamped last_transform_stamped;
    ros::ServiceClient _arm_client;
    ros::ServiceClient _backup_arm_client;
    ros::ServiceClient _mode_client;
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener;
    mavros_msgs::CommandBool arm_cmd;
    mavros_msgs::SetMode mode;
    std::string drone_name;

};




#endif //CONTROLLER_WATCHER_H
