#!/usr/bin/env python
# license removed for brevity
import rospy
import geometry_msgs.msg
import roslib
import tf2_ros
import tf


def talker():
    publish_tf = rospy.get_param('publish_tf', True)
    waypoint = rospy.get_param('waypoint', {'x': 0., 'y': 0., 'z': 0.60})
    x, y, z = waypoint['x'], waypoint['y'], waypoint['z']

    tf_pub = tf2_ros.TransformBroadcaster()

    pub = rospy.Publisher('waypoint', geometry_msgs.msg.Point, queue_size=10)
    rospy.init_node('waypointer')
    rate = rospy.Rate(10)  # 10hz
    while not rospy.is_shutdown():

        if publish_tf:
            t = geometry_msgs.msg.TransformStamped()
            t.header.stamp = rospy.Time.now()
            t.header.frame_id = "origo"
            t.child_frame_id = "waypoint"
            t.transform.translation.x = x
            t.transform.translation.y = y
            t.transform.translation.z = z
            t.transform.rotation.x = 0
            t.transform.rotation.y = 0
            t.transform.rotation.z = 0
            t.transform.rotation.w = 1
            # rospy.loginfo(t)
            tf_pub.sendTransform(t)
        else:
            p = geometry_msgs.msg.Point()
            p.x = 0;
            p.y = 0;
            p.z = 1;

            pub.publish(p)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
