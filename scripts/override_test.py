#!/usr/bin/env python


import argparse
import rospy
from std_msgs.msg import String
import math
import tf2_ros
import geometry_msgs.msg
import tf
import tf.transformations as tfm
import mavros
from mavros_msgs.msg import OverrideRCIn
from mavros_msgs.msg import State
import mavros_msgs
# from mavros_msgs.srv import CommandBool
import numpy as np
from mavros.utils import *
from mavros import command




def altitude_control():
    """Generate altitude Thrust based on some variables and constants


    Returns thrust in newton"""

    # altitude error signal
    global e_alt_der_last
    global e_alt_int_last
    global e_alt_last
    global mass
    global g
    global delta_t
    # alt error signal
    e_alt = trans.transform.translation.z

    # gains
    k_pz = 0.1
    t_iz = np.inf
    t_dz = 0.0
    # time derivative
    t_dlp = 0.025

    # Derivative term
    alpha = (delta_t / (t_dlp + delta_t))
    e_der = alpha * (1.0 / delta_t) * (e_alt - e_alt_last) + (1 - alpha) * e_alt_der_last
    e_int = e_alt_int_last + e_alt * delta_t
    # Confusing equations
    thrust_alt = mass / (math.cos(roll) * math.cos(pitch)) * (g * k_pz) * (e_alt + (1 / t_iz) * e_int + t_dz * e_der)

    # print 'e:     %s' % e_alt
    # print 'e_der: %s' % e_der
    # print 'e_int: %s' % e_int
    # print 'T:     %s' % thrust

    # set last values
    e_alt_int_last = e_int
    e_alt_last = e_alt
    e_alt_der_last = e_der

    return thrust_alt


def yaw_control():
    """Generate yaw Thrust based on some variables and constants


    Returns yaw thrust in newton"""
    global e_yaw_int_last
    global e_yaw_der_last
    global e_yaw_last
    # yaw error signal
    e_yaw = yaw
    # gains
    k_py = 0.1
    t_iy = np.inf
    t_dy = 0.0
    t_dlp = 0.025
    alpha = (delta_t / (t_dlp + delta_t))
    e_der = alpha * (1.0 / delta_t) * (e_yaw - e_yaw_last) + (1 - alpha) * e_yaw_der_last
    e_int = e_yaw_int_last + e_yaw * delta_t

    thrust_yaw = k_py * (e_yaw + (1 / t_iy) * e_int + t_dy * e_der)

    # set last values
    e_yaw_int_last = e_int
    e_yaw_last = e_yaw
    e_yaw_der_last = e_der

    return thrust_yaw


def pos_control():
    """Generate position Thrust based on some variables and constants


    Returns roll and pitch thrust in newton"""
    global e_x_int_last
    global e_x_der_last
    global e_x_last
    global e_y_int_last
    global e_y_der_last
    global e_y_last
    k_pxy = 0.4
    t_ixy = np.inf
    t_dxy = 0.0
    t_dlp = 0.025
    global delta_t

    e_x = trans.transform.translation.x
    e_y = trans.transform.translation.y
    # print 'e_x', e_x
    # print 'e_y', e_y
    alpha = (delta_t / (t_dlp + delta_t))
    e_x_der = alpha * (1.0 / delta_t) * (e_x - e_x_last) + (1 - alpha) * e_x_der_last
    e_x_int = e_x_int_last + e_x * delta_t
    e_y_der = alpha * (1.0 / delta_t) * (e_y - e_y_last) + (1 - alpha) * e_y_der_last
    e_y_int = e_y_int_last + e_y * delta_t
    thrust_pitch = k_pxy * (e_x + (1 / t_ixy) * e_x_int + t_dxy * e_x_der)
    thrust_roll = k_pxy * (e_y + (1 / t_ixy) * e_y_int + t_dxy * e_y_der)
    e_x_int_last = e_x_int
    e_x_last = e_x
    e_x_der_last = e_x_der
    e_y_int_last = e_y_int
    e_y_last = e_y
    e_y_der_last = e_x_der
    thrust_pos = [thrust_roll, thrust_pitch]

    return thrust_pos


def clamp_pwm(pwm_in):
    if pwm_in < 1000:
        return 1000
    elif pwm_in > 2000:
        return 2000
    else:
        return pwm_in


def clamp_pwm_thrust(thrust_pwm_in):
    if thrust_pwm_in < 1000:
        return 1000
    elif thrust_pwm_in > 1300:
        return 1300
    else:
        return thrust_pwm_in


def set_state(data):
    global state
    state = data



if __name__ == '__main__':
    rospy.init_node('tester')
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)
    drone_name = rospy.get_param("drone_name", "papp")
    override_topic = 'mavros_%s/rc/override' % drone_name
    state_topic = 'mavros_%s/state' % drone_name
    pub = rospy.Publisher(override_topic, OverrideRCIn, queue_size=10)
    sub = rospy.Subscriber(state_topic, State, set_state)
    state = State()
    hz = 20
    rate = rospy.Rate(hz)

    # estimate init
    e_alt_int_last, e_alt_last, e_alt_der_last, \
    e_yaw_int_last, e_yaw_last, e_yaw_der_last, \
    e_x_last, e_x_der_last, e_x_int_last, \
    e_y_last, e_y_der_last, e_y_int_last \
        = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    # time per step
    delta_t = 1.0 / hz
    # mass of quad
    mass = 0.7
    # gravity
    g = 9.81
    # thrust to pwm ratio
    k_th = 174.9

    # default pwms
    default_pwm_roll = 1500
    default_pwm_pitch = 1500
    default_pwm_thrust = 1000
    default_pwm_yaw = 1500
    no_flag = False
    throttle_channel = 2
    last_armed = rospy.Time()
    msg = OverrideRCIn()
    rospy.wait_for_service('/mavros_papp/cmd/arming')

    while not rospy.is_shutdown():
        no_flag = False
        try:
            trans = tfBuffer.lookup_transform(drone_name, 'waypoint', rospy.Time())
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            msg.channels = [0, 0, 1000, 0, 0, 0, 0, 1]
            print msg.channels
            # try:
            #     arm = rospy.ServiceProxy('/mavros_papp/cmd/arming', mavros_msgs.srv.CommandBool)
            #     arm(False)
            # except rospy.ServiceException, e:
            #     print "Service call failed: %s" % e
            rate.sleep()
            continue
        q = lambda o: np.array([o.x, o.y, o.z, o.w])
        q1 = q(trans.transform.rotation)
        # print q1
        m = tfm.quaternion_matrix(q1)
        # print m
        roll, pitch, yaw = tf.transformations.euler_from_matrix(m, 'sxyz')
        # print math.degrees(yaw)

        pwm_roll = pos_control()[0] * k_th + 1500
        pwm_pitch = pos_control()[1] * k_th + 1500
        pwm_thrust = altitude_control() * k_th + 1100
        pwm_yaw = yaw_control() * k_th + 1500

        # print 'roll ', clamp_pwm(pwm_roll), 'pitch ', clamp_pwm(pwm_pitch), 'thrust ', clamp_pwm_thrust(
        #   pwm_thrust), 'yaw ', clamp_pwm(pwm_yaw)

        msg = OverrideRCIn()



        if state.armed == True:
            last_armed = rospy.Time()
            msg.channels = [clamp_pwm(pwm_roll), clamp_pwm(pwm_pitch), clamp_pwm_thrust(pwm_thrust), 0, 0, 0, 0, 0]
            # msg.channels = [0, clamp_pwm(pwm_pitch), 0, 0, 0, 0, 0, 0]
            print msg.channels
        elif state.armed == False:
            last_armed = rospy.Time()
            msg.channels = [0, 0, 1000, 0, 0, 0, 0, 0]
            print msg.channels
        if rospy.Time() - last_armed > rospy.Duration(1.0):
            msg.channels = [0, 0, 1000, 0, 0, 0, 0, 0]
            print msg.channels
        pub.publish(msg)
        rate.sleep()
