#!/usr/bin/env python
import rospy

import math
import tf2_ros
import geometry_msgs.msg
import tf2_py

if __name__ == '__main__':
    rospy.init_node('velocity')

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    t = 1.0 / 200
    x_0 = 0
    y_0 = 0
    z_0 = 0

    vel = rospy.Publisher('/vel', geometry_msgs.msg.Twist, queue_size=1)

    rate = rospy.Rate(200)
    while not rospy.is_shutdown():
        try:
            trans = tfBuffer.lookup_transform('origo', 'papp', rospy.Time())
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rate.sleep()
            continue
        x = trans.transform.translation.x
        y = trans.transform.translation.y
        z = trans.transform.translation.z
        d = math.sqrt((x ** 2 + x_0 ** 2) + (y ** 2 + y_0 ** 2) + (z ** 2 + z_0 ** 2))
        v = d / t
        # print v
        msg = geometry_msgs.msg.Twist()

        msg.linear.x = (abs(x) - abs(x_0)) / t
        msg.linear.y = (abs(y) - abs(y_0)) / t
        msg.linear.z = (abs(z) - abs(z_0)) / t

        #      msg.angular.z = 4 * math.atan2(trans.transform.translation.y, trans.transform.translation.x)
        #      msg.linear.x = 0.5 * math.sqrt(trans.transform.translation.x ** 2 + trans.transform.translation.x ** 2)

        vel.publish(msg)

        x_0 = x
        y_0 = y
        z_0 = z

        rate.sleep()
